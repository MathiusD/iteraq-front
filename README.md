# IterαQ - Section Web/Front

Ce dépôt est le dépôt consacré au développement de la vitrine web de l'équipe IterαQ au sein de la nightcode 2020.

## Auteurs

* Théo Denis
* Valentin Celles
* Clément Vienne
* Aefka
* Féry Mathieu